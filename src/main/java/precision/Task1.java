package precision;

import java.math.BigDecimal;

public class Task1 {

    // Вы столкнулись с проблемой точности вычислений. Есть метод:
    //
    //    private static double getSum(double a, double b) {
    //        return a + b;
    //    }
    //Передав в него два числа для важного расчета 3000.1 и 200.2
    // вы с ужасом обнаруживаете что сумма чисел равна 3200.2999999999997.
    //
    //1) Перепишите этот код, для того чтобы вычисления возвращали правильный результат.
    //2) Добавьте возможность передавать на вход методу данные в ЛЮБОМ формате (любой класс или примитив),
    // не теряя при этом в точности вычислений.
    //В случае если не получилось вытащить число из переданного объекта, выбрасывайте исключение IllegalArgumentException.

    public static void main(String[] args) {
        double sumDoubles = getSum(3000.1, 200.2);
        System.out.println("Sum with doubles: " + sumDoubles);

        double sumBigDecimals = getSumCorrect(new BigDecimal("3000.1"), new BigDecimal("200.2"));
        System.out.println("Sum with big decimals: " + sumBigDecimals);

        double sumUniversal = getSumUniversal("3000.1", "200.2");
        System.out.println("Sum with objects: " + sumUniversal);
    }

    private static double getSum(double a, double b) {
        return a + b;
    }

    private static double getSumCorrect(BigDecimal a, BigDecimal b) {
        return a.add(b).doubleValue();
    }

    private static double getSumUniversal(Object a, Object b) {
        if (a == null || b == null || !isNumber(a.toString()) || !isNumber(b.toString())) {
            throw new IllegalArgumentException("Arguments should be numbers");
        }
        // делаем явное преобразование в строку для сохранения точности
        String aAsString = a.toString();
        String bAsString = b.toString();
        // создаем объекты BigDecimal из строк
        BigDecimal aBigDecimal = new BigDecimal(aAsString);
        BigDecimal bBigDecimal = new BigDecimal(bAsString);
        // возвращаем сумму в виде double
        return aBigDecimal.add(bBigDecimal).doubleValue();
    }

    private static boolean isNumber(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }
        boolean hasDigits = false;
        boolean hasDecimalPoint = false;
        for (int i = 0; i < s.length(); i++) {
            char symbol = s.charAt(i);
            if (symbol == '.') {
                // строка должна содержать не более одного дробного разделителя
                if (hasDecimalPoint) {
                    return false;
                }
                hasDecimalPoint = true;
            } else if (Character.isDigit(symbol)) {
                // строка должна содержать хотя бы одну цифру
                hasDigits = true;
            } else {
                // если символ не цифра и не дробный разделитель ".", то входная строка это не число
                return false;
            }
        }
        return hasDigits;
    }
}
