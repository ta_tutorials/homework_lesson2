package equalscontracts;

import equalscontracts.util.Person;

public class Task1 {

    //Взять класс Person из предыдущего урока (поля age, name, surname)
    // переопределить для него equals и hashcode с учетом всех полей класса
    //(т.е. объекты будут равны в случае если поля равны).
    //
    //Попробовать на практике в методе main программы сравнить с null, с другим объектом, с самим собой; получить hashcode

    public static void main(String[] args) {
        Person person1 = new Person(25, "John", "Doe");
        Person person2 = new Person(25, "John", "Doe");
        Person person3 = new Person(30, "Jane", "Doe");

        System.out.println("person1 equals null: " + person1.equals(null));
        System.out.println("person1 equals person2: " + person1.equals(person2));
        System.out.println("person1 equals person1: " + person1.equals(person1));
        System.out.println("person1 equals person3: " + person1.equals(person3));

        System.out.println("person1 hashcode: " + person1.hashCode());
        System.out.println("person2 hashcode: " + person2.hashCode());
    }
}
