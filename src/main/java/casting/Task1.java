package casting;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

public class Task1 {

    // Есть метод, принимающий аргументом на вход ArrayList и получает первый найденный элемент,
    // удовлетворяющий произвольному условию (условие придумать самим или использовать Predicate на входе метода c условием).
    // Появилось требование работать еще и с HashSet с такой же логикой, напишите код, который будет это делать,
    // при этом, стараясь избегать дублирования кода.

    public static void main(String[] args) {
        // ArrayList
        Collection<Integer> arrayList = List.of(1, 2, 3, 4, 5);
        Integer firstElementFromArrayList = getFirstElementByCondition(arrayList, element -> element > 3);
        System.out.println("First element from ArrayList: " + firstElementFromArrayList);

        // HashSet
        Collection<Integer> hashSet = Set.of(1, 2, 3, 4, 5);
        Integer firstElementFromHashSet = getFirstElementByCondition(hashSet, element -> element > 3);
        System.out.println("First element from HashSet: " + firstElementFromHashSet);
    }

    private static <T> T getFirstElementByCondition(Collection<T> list, Predicate<T> predicate) {
        return list.stream()
                .filter(predicate)
                .findFirst()
                .orElse(null);
    }
}
